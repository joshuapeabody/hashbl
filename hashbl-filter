#!/usr/bin/perl

# hashbl-filter for MIMEDefang

use strict;
use warnings;

# Require Perl version >= 5.10.1 for safety.
# Check with perl --version or print $]
# WARNING: Comment out at your own risk!
use 5.010001;

# In Perl base so always available.
use IO::File;

# Add current working directory (.) to @INC for HASHBL check/tests.
use Cwd;
use lib cwd();

our $VERSION = 1.0;

#####################################################################
# ------------- START COPY HERE FOR MIMEDEFANG FILTER ------------- #
#####################################################################

our ($HBL);

sub filter_initialize {
  # The HASHBL.pm Perl Module MUST be in Perl @INC path.
  # Typically installed in /usr/local/share/perl5/
  use HASHBL;
  $HBL = HASHBL->new();
  return;
}

# For this check to work, you must use the mimedefang -s flag AKA MX_SENDER_CHECK in sysconfig.
# This must return a two-to-five element list: ($code, $msg, $smtp_code, $smtp_dsn, $delay)
sub filter_sender {
  my ($sender, $ip, $hostname, $helo) = @_;
  # Only check envelope email address when not empty.
  if ($sender) {
    my $result = $main::HBL->query($sender);
    # Only reject when there is a valid result.
    if ($result) {
      md_syslog('warning', "Rejecting sender $sender because of HASHBL result $result");
      action_bounce($result);
      return('REJECT', $result);
    }
  }
  # Fallback to continue.
  return('CONTINUE', q{});
}

# Filter message headers in DATA section.
sub filter_end {
  my ($entity) = @_; # Ignored.
  # No point in doing work if message was already rejected.
  return if (message_rejected());
  my $headersFName = './HEADERS';
  if (!-e $headersFName) {
    # Headers file does not exist.
    md_syslog('warning', 'HEADERS file does not exist.');
    return('CONTINUE', q{}); # Keep processing.
  } else {
    # Open the headers file in read-only mode.
    my $headersFH = IO::File->new($headersFName, 'r');
    if (!defined $headersFH) {
      md_syslog('warning', 'HEADERS file cannot be opened read-only.');
      return('CONTINUE', q{}); # Keep processing.
    }
    while (<$headersFH>) {
      # Headers are one-per-line, not folded to multiple.
      # Only look at possible author/return/sender headers.
      if (/^(Disposition\-Notification\-To|From|Reply\-To|Return\-Path|Sender):\s+(.+)/i) {
        my $headerTag = $1;
        my $headerData = $2;
        my $result = $main::HBL->query($headerData);
        # Only reject when there is a valid result.
        if ($result) {
          md_syslog('warning', "Rejected $headerTag header because of HASHBL result $result");
          action_bounce($result);
        }
      }
    }
    seek $headersFH, 0, 0 or md_syslog('warning', "Cannot seek to beginning of headers file $headersFName.");
    close $headersFH;
  }
  # Fallback to continue, keep processing.
  return('CONTINUE', q{});
}

# To validate run:  mimedefang.pl -f hashbl-filter -validate
# This must return a simple return code, 0=success.
sub filter_validate {
  filter_initialize(); # Must init first.
  $main::InMessageContext = 1; # Pretend this is real.
  # example.com from RFC2606 <https://tools.ietf.org/html/rfc2606>
  # TEST-NET-3 from RFC5737 <https://tools.ietf.org/html/rfc5737>
  my @resultBad = filter_sender('noemail@example.com', '208.0.113.32', 'mail.example.com', 'mail.example.com');
  # Expect reject.
  if ($resultBad[0] ne 'REJECT') {
    warn 'Validate filter: Bad Sender Failed! ' . $resultBad[0] . "\n";
    return 1; # Failure
  }

  main::init_globals();
  filter_initialize();
  $main::InMessageContext = 1;
  my @resultGood = filter_sender('cleansender@greatorg.example', '208.0.113.32', 'mail.example.com', 'mail.example.com');
  # Expect continue.
  if ($resultGood[0] ne 'CONTINUE') {
    warn 'Validate filter: Good Sender Failed!  ' . $resultGood[1] . "\n";
    return 1; # Failure
  }

  main::init_globals();
  filter_initialize();
  $main::InMessageContext = 1;
  # Good set of headers.
  # Testing filter_end requires a ./HEADERS file, filled with example data.
  my $goodFH = IO::File->new('./HEADERS', 'w');
  if ($goodFH) {
    print {$goodFH} <<'END_GOOD_HEADERS';
From: user (fake.user@email.invalid)
To: noemail@example.com
Return-Path: <fake.user@email.invalid>
Envelope-To: noemail@example.com
END_GOOD_HEADERS
    close $goodFH;
    filter_end();
    $goodFH = undef;
    if (!unlink './HEADERS') {
      warn "WARNING: Could not delete ./HEADERS in good headers test, continuing anyway.\n";
    }
    # Expect accepted.
    if (defined $main::Actions{'bounce'}) {
      warn "Validate filter: Good Headers Failed!\n";
      return 1; # Failure
    }
  }

  main::init_globals();
  filter_initialize();
  $main::InMessageContext = 1;
  # Testing filter_end requires a ./HEADERS file, filled with example data.
  my $badFH = IO::File->new('./HEADERS', 'w');
  if ($badFH) {
    print {$badFH} <<'END_BAD_HEADERS';
Return-Path: fake.address@no-such.invalid
From: Bad Sender Test <noemail@example.com>
To: innocent.victim@domain.example
END_BAD_HEADERS
    close $badFH;
    filter_end();
    $badFH = undef;
    if (!unlink './HEADERS') {
      warn "WARNING: Could not delete ./HEADERS in bad headers test, continuing anyway.\n";
    }
    # Expect rejected.
    if (!defined $main::Actions{'bounce'}) {
      warn "Validate filter: Bad Headers Failed!\n";
      return 1; # Failure
    }
  } else {
    warn "WARNING: Could not open ./HEADERS for writing in bad headers test, continuing anyway.\n";
  }

  # All test results were as expected.
  warn "Validate filter: Success.\n";
  return 0; # Success
}

#####################################################################
# -------------- END COPY HERE FOR MIMEDEFANG FILTER -------------- #
#####################################################################

# Required for -test to work.
1;

# vim: filetype=perl
