#!/usr/bin/perl

use strict;
use warnings;

# Require Perl version >= 5.10.1 for safety.
# Check with perl --version or print $]
# WARNING: Comment out at your own risk!
use 5.010001;

# Digest::SHA - Perl extension for SHA-1/256/...
# Older, deprecated alternative PM is Digest::SHA1
# RPM names: perl-Digest-SHA  perl-Digest
use Digest::SHA qw(sha1_hex);

# Email::Address - RFC 2822 Address Parsing and Creation
# https://metacpan.org/pod/Email::Address
# RPM name: perl-Email-Address
# http://dries.eu/rpms/perl-Email-Address/perl-Email-Address
use Email::Address;

use English qw(-no_match_vars);

# Getopt::Long - Extended processing of command line options
# https://metacpan.org/pod/Getopt::Long
# RPM name: perl-Getopt-Long
use Getopt::Long 2.33 qw(GetOptions);
# Enable Auto Help/Version and use GNU getopt long syntax.
Getopt::Long::Configure qw(auto_help auto_version gnu_getopt);

# Net::DNS - Perl interface to the Domain Name System
# https://metacpan.org/pod/Net::DNS
# RPM name: perl-Net-DNS
use Net::DNS;

our $VERSION = '1.2.2';

# Get CLI options
our (%CLEANEA, $DEBUG, $DNSZONE, $NOQUERY, $RES, $VERBOSE, %WHITELIST, $WLFILE);
GetOptions(
  'debug|D' => \$DEBUG,
  'dnszone|d=s' => \$DNSZONE,
  'noquery|n' => \$NOQUERY,
  'verbose|v' => \$VERBOSE,
  'whitelist|w=s' => \$WLFILE
) or die "Usage: $PROGRAM_NAME [--debug] [--dnszone=zone.example.org] [--help] [--noquery] [--verbose] [--version] [--whitelist=WLFILE]\n";
# MSBL EBL is default DNS Zone to query, if not set on CLI.
# https://msbl.org/docs/ebl-info.pdf
$DNSZONE //= 'ebl.msbl.org';

# Open and read in the whitelist file.
if ($WLFILE) {
  open my $wl, '<', $WLFILE or warn "WARNING: Could not open whitelist file $WLFILE: $OS_ERROR\n";
  %WHITELIST = map { chomp; lc => 1 } <$wl>; # File lines to hash keys.
  close $wl or warn "WARNING: Could not close whitelist file $WLFILE: $OS_ERROR\n";
  warn 'INFO: Loaded domain whitelist file ', $WLFILE, ' with ',
    scalar keys %WHITELIST, " entries.\n" if (keys %WHITELIST > 0 && $VERBOSE);
}

# Setup the DNS resolver.
# https://metacpan.org/pod/Net::DNS::Resolver
$RES = Net::DNS::Resolver->new;
#$RES->debug($DEBUG); # DNS query debug. SUPER VERBOSE!!!
$RES->defnames(0); # Do not append default domain to name.
$RES->udp_timeout(5); # Set UDP timeout to 5 seconds.
$RES->persistent_udp(1); # Use 1 UDP socket multiple times.

# Parse all email addresses from STDIN or input file.
while (<>) {
  chomp;
  my @EA = Email::Address->parse($_);
  foreach my $addr (@EA) {
    # Lower-case the address parts.
    my $cleanLocal = lc $addr->user;
    my $cleanDomain = lc $addr->host;
    my $FQDN = 1;
    next if (!($cleanLocal && $cleanDomain)); # Sanity check.
    $cleanLocal =~ s/[+]\S*$//g; # Silently remove + tags from user.
    # Silently remove all periods from Google Mail/Gmail user.
    $cleanLocal =~ s/[.]//g if ($cleanDomain =~ m/^(gmail|googlemail)[.]com/i);
    # Silently change Google Mail to Gmail domain.
    $cleanDomain = 'gmail.com' if ($cleanDomain eq 'googlemail.com');
    # Silently remove mail log prefixes from user that Email::Address won't clean up.
    $cleanLocal =~ s/^(envelope\-from|id|r|receiver)\=//gi; # TODO: Find a better way.
    # Skip all common role account local parts which will not be listed.
    # Questionable users: admin billing devnull dns ftp help info list nobody
    # ???  noc nntp null orders sales ssladmin support undisclosed-recipients
    if ($cleanLocal =~ m/^(abuse|(ana)?cron|(host|post|web)master|mail(er\-)?daemon|root)$/i) {
      warn "INFO: Common role account $cleanLocal\@$cleanDomain may be whitelisted, skipping!\n" if ($VERBOSE);
      next;
    }
    # Skip if email address domain is dot-less, <=3, a local, reserved or test TLD.
    # https://www.ietf.org/rfc/rfc2606.txt
    # https://tools.ietf.org/html/draft-cheshire-homenet-dot-home-02
    if ($cleanDomain !~ m/[.]/i || length $cleanDomain <= 3 ||
        $cleanDomain =~ m/[.](example|home|invalid|lan|local(host)?|test)$/i) {
      # TODO: Query Mozilla Public Suffix List to validate TLDs instead.
      # https://publicsuffix.org/  https://metacpan.org/pod/Mozilla::PublicSuffix
      warn "INFO: Domain $cleanDomain may not be a valid Internet FQDN, skipping!\n" if ($VERBOSE);
      $FQDN = 0;
      next;
    }
    # Check the clean domain part (only) against the whitelist file.
    if (keys %WHITELIST > 0 && $WHITELIST{$cleanDomain}) {
      warn "DEBUG: Domain $cleanDomain is in whitelist, skipping!\n" if ($DEBUG);
      next;
    }
    # Create the cleaned version of the email address.
    my $cleanAddr = $cleanLocal . q{@} . $cleanDomain;
    warn "DEBUG: Address $addr cleaned to $cleanAddr\n" if ($DEBUG);
    # Adding to a hash will automatically eliminate duplicates.
    $CLEANEA{$cleanAddr} = $FQDN;
  }
}

warn 'INFO: Clean email addresses total ',
  scalar keys %CLEANEA, " entries.\n" if (keys %CLEANEA > 0 && $VERBOSE);

while (my ($cleanAddr, $FQDN) = each %CLEANEA) {
  # Calculate the SHA-1 hex hash of the clean email address.
  my $sha1 = sha1_hex($cleanAddr);
  next if (!$sha1); # Sanity check.
  my $name = $sha1 . q{.} . $DNSZONE; # FQDN query
  warn "INFO: Cleaned address $cleanAddr will query DNS name $name\n" if ($VERBOSE);
  # If --noquery was passed then skip the rest and do not query DNS.
  # Most useful with --debug option too.
  next if ($NOQUERY);
  my $reply = $RES->query($name, 'A');
  if ($reply) {
    foreach my $rr (grep {$_->type eq 'A'} $reply->answer) {
      my $ipCode = $rr->address; # IP code is an IPv4 address.
      my $text = q{};
      if ($ipCode) { # Any IP code was returned.
        my $txtReply = $RES->query($name, 'TXT');
        if ($txtReply) { # TXT query might not return.
          foreach my $rr2 (grep {$_->type eq 'TXT'} $txtReply->answer) {
            $text .= $rr2->txtdata; # Append TXT return data.
          }
        }
      }
      # If/when A query returns multiple IP codes then this will print multiple lines.
      print $cleanAddr, ' is listed in ', $DNSZONE, ' with ', $ipCode, q{ }, $text, "\n";
    }
  } else {
    # NXDOMAIN (no result) is considered a DNS query failure.
    # Do not warn about this failure unless debug AND verbose, and even questionable then.
    warn 'DEBUG WARNING: DNS query failed for name ', $name, q{ }, $RES->errorstring, "\n" if ($DEBUG && $VERBOSE);
  }
}

1;

__END__

=pod

=head1 NAME

query-hashbl.pl - Query HASHBL via DNS lookups

=head1 USAGE

S<query-hashbl.pl [B<--debug|-D>] [B<--dnszone|-d=I<zone.example.org>>] [B<--help|-?>] [B<--noquery|-n>] [B<--verbose|-v>] [B<--version>] [B<--whitelist|-w=I<WLFILE>>]>

=head1 OPTIONS

=over

=item  --debug OR -D       Print noisy debugging messages to STDERR.

=item  --dnszone=z.o.n.e   Use DNS zone for HASHBL lookups. OR -d=z.o.n.e Uses the MSBL EBL by default, if not set.

=item  --help OR -?        Print this usage. Try: perldoc query-hashbl.pl

=item  --noquery OR -n     Do not query DNS, local-only. Use with --debug.

=item  --verbose OR -v     Print chatty informational messages to STDERR.

=item  --version           Print versions of program, GetOpt::Long and Perl.

=item  --whitelist=WLFILE  Read WLFILE file for domain names to skip/ignore. OR -w=WLFILE Use only entire lowercase domain name, one per line.

=item  Input is read via filename on CLI or STDIN (pipe, redirect).

=back

=head1 EXAMPLES

echo -n 'noemail@example.com' | query-hashbl.pl

query-hashbl.pl --debug --noquery --verbose /var/log/maillog 2E<gt>&1 | less

query-hashbl.pl --whitelist=mydomains.txt suspicious-emails.txt

=head1 BUGS AND LIMITATIONS

The domain whitelist option only works on entire lowercase domain name.
One entry per line, do not use regular expressions or comments.

=head1 NOTES

If an email address that you expect to be listed is not being returned,
then enable debug and verbose options. May be skipped due to whitelisting.

=head1 AUTHOR

Joshua Peabody E<lt>joshua.peabody@protonmail.comE<gt>

=head1 LICENSE AND COPYRIGHT

Licensed under The Artistic License 2.0, same as Perl. Read LICENSE.txt.

=cut

