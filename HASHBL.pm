package HASHBL;

use strict;
use warnings;

# Require Perl version >= 5.10.1 for safety.
# Check with perl --version or print $]
# WARNING: Comment out at your own risk!
use 5.010001;

# Digest::SHA - Perl extension for SHA-1/256/...
# Older, deprecated alternative PM is Digest::SHA1
# RPM names: perl-Digest-SHA  perl-Digest
use Digest::SHA qw(sha1_hex);

# Email::Address - RFC 2822 Address Parsing and Creation
# https://metacpan.org/pod/Email::Address
# RPM name: perl-Email-Address
# http://dries.eu/rpms/perl-Email-Address/perl-Email-Address
use Email::Address;

use English qw(-no_match_vars);

# Net::DNS - Perl interface to the Domain Name System
# https://metacpan.org/pod/Net::DNS
# RPM name: perl-Net-DNS
use Net::DNS;

# Ignore experimental warnings about using ~~ smartmatch op.
no if $] >= 5.017011, warnings => 'experimental::smartmatch';

our $VERSION = '2.0';

sub new {
  my ($class, %params) = @_;
  my $self = {};
  bless $self, $class;

  if ($params{'dnszone'}) {
    $self->{'dnszone'} = $params{'dnszone'};
  } else {
    # MSBL EBL is the default DNS Zone to query, if not passed in.
    # https://msbl.org/docs/ebl-info.pdf
    $self->{'dnszone'} = 'ebl.msbl.org';
  }

  if ($params{'dnscodes'}) {
    # Check that an array reference was passed in.
    if (ref($params{'dnscodes'}) eq 'ARRAY') {
      # Copy the array reference.
      $self->{'dnscodes'} = $params{'dnscodes'};
    } else {
      # Operator error, warn and (try to) continue.
      warn 'WARNING: The dnscodes parameter is not an array reference!' . "\n";
    }
  } else {
    # No dnscodes param passed, explicitly set dnscodes to only
    # process DNS response codes 127.0.0.2 and 127.0.0.3 by default.
    my @defaultCodes = ('127.0.0.2', '127.0.0.3');
    $self->{'dnscodes'} = \@defaultCodes;
  }

  if ($params{'whitelist'}) {
    my $wlFile = $params{'whitelist'};
    if ($wlFile) {
# Open and read in the whitelist file.
      open my $wl, '<', $wlFile or warn "WARNING: Could not open whitelist file $wlFile: $OS_ERROR\n";
      %{ $self->{'whitelist'} } = map { chomp; lc => 1 } <$wl>; # File lines to hash keys.
      close $wl or warn "WARNING: Could not close whitelist file $wlFile: $OS_ERROR\n";
      warn 'INFO: Loaded domain whitelist file ', $wlFile, ' with ',
           scalar keys %{ $self->{'whitelist'} }, " entries.\n" if (keys %{ $self->{'whitelist'} } > 0 && $self->{'verbose'});
    }
  }

  $self->{'debug'} = 1 if ($params{'debug'});
  $self->{'noquery'} = 1 if ($params{'noquery'});
  $self->{'verbose'} = 1 if ($params{'verbose'});

# Setup the DNS resolver.
# https://metacpan.org/pod/Net::DNS::Resolver
  $self->{'res'} = Net::DNS::Resolver->new;
#$self->{'res'}->debug($self->{'debug'}); # DNS query debug. SUPER VERBOSE!!!
  $self->{'res'}->defnames(0); # Do not append default domain to name.
    $self->{'res'}->udp_timeout(5); # Set UDP timeout to 5 seconds.
    $self->{'res'}->persistent_udp(1); # Use 1 UDP socket multiple times.

    return $self;
}

# Parse all email addresses from parameters.
sub query {
  my ($self, @addresses) = @_;
  my @EA = Email::Address->parse(@addresses);
  my (%cleanEA);

  foreach my $addr (@EA) {
# Lower-case the address parts.
    my $cleanLocal = lc $addr->user;
    my $cleanDomain = lc $addr->host;
    my $FQDN = 1;
    next if (!($cleanLocal && $cleanDomain)); # Sanity check.
      $cleanLocal =~ s/[+]\S*$//g; # Silently remove + tags from user.
# Silently remove all periods from Google Mail/Gmail user.
      $cleanLocal =~ s/[.]//g if ($cleanDomain =~ m/^(gmail|googlemail)[.]com/i);
# Silently change Google Mail to Gmail domain.
    $cleanDomain = 'gmail.com' if ($cleanDomain eq 'googlemail.com');
# Silently remove mail log prefixes from user that Email::Address won't clean up.
    $cleanLocal =~ s/^(envelope\-from|id|r|receiver)[=]//gi;
# Skip all common role account local parts which will not be listed.
# Questionable users: admin billing devnull dns ftp help info list nobody
# ???  noc nntp null orders sales ssladmin support undisclosed-recipients
      if ($cleanLocal =~ m/^(abuse|(ana)?cron|(host|post|web)master|mail(er\-)?daemon|root)$/i) {
        warn "INFO: Common role account $cleanLocal\@$cleanDomain may be whitelisted, skipping!\n" if ($self->{'verbose'});
        next;
      }
# Skip if email address domain is dot-less, <=3, a local, reserved or test TLD.
# https://www.ietf.org/rfc/rfc2606.txt
# https://tools.ietf.org/html/draft-cheshire-homenet-dot-home-02
    if ($cleanDomain !~ m/[.]/i || length($cleanDomain) <= 3 ||
        $cleanDomain =~ m/[.](example|home|invalid|lan|local(host)?|test)$/i) {
# TODO: Query Mozilla Public Suffix List to validate TLDs instead.
# https://publicsuffix.org/  https://metacpan.org/pod/Mozilla::PublicSuffix
      warn "INFO: Domain $cleanDomain may not be a valid Internet FQDN, skipping!\n" if ($self->{'verbose'});
      $FQDN = 0;
      next;
    }
# Check the clean domain part (only) against the whitelist file.
    if ((keys(%{ $self->{'whitelist'} }) > 0) && ($self->{'whitelist'}{$cleanDomain})) {
      warn "DEBUG: Domain $cleanDomain is in whitelist, skipping!\n" if ($self->{'debug'});
      next;
    }
# Create the cleaned version of the email address.
    my $cleanAddr = $cleanLocal . q{@} . $cleanDomain;
    warn "DEBUG: Address $addr cleaned to $cleanAddr\n" if ($self->{'debug'});
# Adding to a hash will automatically eliminate duplicates.
    $cleanEA{$cleanAddr} = $FQDN;
  }

  warn 'INFO: Clean email addresses total ',
       scalar keys %cleanEA, " entries.\n" if (keys %cleanEA > 0 && $self->{'verbose'});

  while (my ($cleanAddr, $FQDN) = each %cleanEA) {
# Calculate the SHA-1 hex hash of the clean email address.
    my $sha1 = sha1_hex($cleanAddr);
    next if (!$sha1); # Sanity check.
    my $name = $sha1 . q{.} . $self->{'dnszone'}; # FQDN query
    warn "INFO: Cleaned address $cleanAddr will query DNS name $name\n" if ($self->{'verbose'});
# If noquery was passed then skip the rest and do not query DNS.
# Most useful with debug param too.
    next if ($self->{'noquery'});
    my $reply = $self->{'res'}->query($name, 'A');
    if ($reply) {
      foreach my $rr (grep {$_->type eq 'A'} $reply->answer) {
        my $ipCode = $rr->address; # IP code is an IPv4 address.
        my $text = q{};
        if (defined($self->{'dnscodes'}) && # If dnscodes param was passed AND
             ($ipCode ~~ @{ $self->{'dnscodes'} })) { # Smart matched in array
          my $txtReply = $self->{'res'}->query($name, 'TXT');
          if ($txtReply) { # TXT query might not return.
            foreach my $rr2 (grep {$_->type eq 'TXT'} $txtReply->answer) {
              $text .= $rr2->txtdata; # Append TXT return data.
            }
          }
          # Only return the first detected IP code and TXT reply, then exits sub.
          return $cleanAddr . ' is listed in ' . $self->{dnszone} . ' with ' . $ipCode . q{ } . $text;
        } else {
          warn "INFO: DNS response code $ipCode did not match dnscodes parameter.\n" if ($self->{'verbose'});
        }
      }
    } else {
# NXDOMAIN (no result) is considered a DNS query failure.
# Do not warn about this failure unless debug AND verbose, and even questionable then.
      warn 'DEBUG WARNING: DNS query failed for name ', $name, q{ }, $self->{'res'}->errorstring, "\n" if ($self->{'debug'} && $self->{'verbose'});
    }
  }
  # No match so return empty string which is false.
  return q{};
}

1;

__END__

=pod

=head1 NAME

HASHBL - Query HASHBL via DNS lookups

=head1 SYNOPSIS

	use HASHBL;
	print HASHBL::query('noemail@example.com') . "\n";

=head1 DESCRIPTION

Input via object-orientated parameters to new, see EXAMPLES.
Output via string if found, else blank (false) if not found.

=head1 PARAMETERS

 new(
   debug => 1,    # Print noisy debugging messages to STDERR.
   dnscodes => \@ipCodes, # Limit DNS response codes to process when
                  # returned from the DNS zone, pass array reference.
                  # Uses 127.0.0.2 and 127.0.0.3 by default, if not set.
   dnszone => 'hashbl.example.org', # Use DNS zone for HASHBL lookups.
                  # Uses the MSBL EBL by default, if not set.
   noquery => 1,  # Do not query DNS, local-only. Use with debug.
   verbose => 1,  # Print chatty informational messages to STDERR.
   whitelist=>$wl # Read $wl file for domain names to skip/ignore.
 );               # Use only entire domain name, one per line.

=head1 EXAMPLES

Use another HASHBL DNS zone, only process 127.0.1.[23] IP codes,
enable debugging and verbosity:

	require HASHBL;
        my @ipCodes = ('127.0.1.2', '127.0.1.3');
	my $hbl = new HASHBL(dnszone => 'myhashbl.example.org',
          dnscodes => \@ipCodes, debug => 1, verbose => 1);
	my $r = $hbl->query($_);
	if ($r) print "Bingo! Hit $r\n";

Use a local whitelist of domains:

	use HASHBL;
	my $hbl = new HASHBL(whitelist => "path/to/safe-domains.txt");
	...

=head1 BUGS AND LIMITATIONS

The domain whitelist param only works on entire (case-insensitive) domain names.
One entry per line, do not use @ sign, regular expressions or comments.

Pass in dnscodes param as a reference to an array of strings. i.e. \\@ipCodes
where @ipCodes = ('127.0.0.2', '127.0.0.3', '127.0.1.2', '127.0.1.3');

=head1 NOTES

If an email address that you expect to be listed is not being returned,
then enable debug and verbose params. May be skipped due to whitelist
and/or dnscodes parameters.

=head1 AUTHOR

Joshua Peabody <joshua.peabody@protonmail.com>

=head1 LICENSE

Licensed under The Artistic License 2.0, same as Perl. Read LICENSE.txt.

=cut

