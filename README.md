# HASHBL Perl code for MIMEDefang milter #

### What is this repository for? ###

* Quick summary

Perl milter code to query a [HASHBL](https://www.msbl.org/hashbls.html) SHA1 hash-based DNS blacklist and block messages based on response codes.

Runs via [MIMEDefang](https://mimedefang.org/) milter for before-queue filtering at the MAIL FROM SMTP dialogue level.

By default, only queries the [MSBL EBL](https://www.msbl.org/ebl.html) with 127.0.0.2 and 127.0.0.3 response codes.
However, the configuration is flexible enough to use other SHA1 hash-based blacklists and process other response codes.

### How do I get set up? ###

* Summary of set up

1. [Download](https://bitbucket.org/joshuapeabody/hashbl/downloads/) or [clone git repo](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html)
2. Ensure all the below dependencies are installed.
3. Read the help/POD documentation.
4. Install into MIMEDefang configuration.
5. Run checks/tests.

### Installing MIMEDefang ###

On RPM-based systems (RHEL, CentOS, etc), the `sendmail-milter` package
is in the *optional* yum repo. If you get a yum error when installing
the `mimedefang` package then enable the optional yum repo and re-install.

Also on RPM-based systems, the `mimedefang` package is in the *EPEL* yum repo.
Again, if you see a yum error when installing the `mimedefang` package then
install and setup [EPEL](https://fedoraproject.org/wiki/EPEL) and re-install.

### Dependencies _RPM package name_ ###

1. MIMEDefang _mimedefang_

2. Perl (`version >= 5.10.1`) _perl_

3. Sendmail Milter library (`libmilter`) _sendmail-milter_

4. Standard make utility _make_

Perl Modules:

1. `Digest::SHA` *perl-Digest-SHA*

2. `Email::Address` *perl-Email-Address*

3. `Getopt::Long` *perl-Getopt-Long*

4. `Net::DNS` *perl-Net-DNS*

5.  POD::Checker  *perl-Pod-Checker*

6.  Perl::Critic  *perl-Perl-Critic*

### How to run checks and tests ###

    make check

Run `perldoc HASHBL.pm` and read *EXAMPLES* section.

Run `./checker` and `perldoc query-hashbl.pl` as well.

### Deployment instructions ###

Run  `make install`  that copies `HASHBL.pm` into `/usr/local/share/perl5`
which by default should be in the Perl library search path.
Hint: Run  `perl -V`  to see Perl `@INC` directories.

Copy/merge contents of `hashbl-filter` into `/etc/mail/mimedefang-filter` file.
This is a *MANUAL* step but without it, the before-queue HASHBL milter won't work!

Edit MIMEDefang configuration file `/etc/sysconfig/mimedefang` and set:

    SOCKET=inet:16543
    MX_SENDER_CHECK=yes

Enable and start MIMEDefang daemons/services:

    systemctl enable --now mimedefang.service
    systemctl enable --now mimedefang-multiplexor.service

If MIMEDefang daemon/service does not start then check inet `SOCKET` TCP port.
You might need to pick another (higher) TCP port number! Run  `netstat -anp`

Enable MIMEDefang milter in `/etc/postfix/main.cf` , read
[Postfix SMTP-Only Milter applications](http://www.postfix.org/MILTER_README.html#smtp-only-milters)

    smtpd_milters = inet:localhost:16543

Verify TCP port number matches what `SOCKET` is set to in `/etc/sysconfig/mimedefang` .

    postfix check

    systemctl restart postfix.service

### Who do I talk to? ###

* [Joshua Peabody](mailto:joshua.peabody@protonmail.com)

* [HASHBL team](https://www.msbl.org/hashbls.html)

