ECHO := echo
INSTALL := install
HBL_FILTER = hashbl-filter
MIMEDEFANG_FILTER = /etc/mail/mimedefang-filter
MIMEDEFANG_PL = mimedefang.pl
PERL_MODULES = HASHBL.pm
CHECKPERL = checker query-hashbl.pl $(HBL_FILTER) $(PERL_MODULES)
PERL := perl
PERLCRITIC := perlcritic
PODCHECKER := podchecker
PM_INSTALL_PATH := /usr/local/share/perl5

.PHONY: all check install

all: check

# Check Perl syntax, test and validate HASHBL MIMEDefang milter.
check:
	@-$(foreach p, $(CHECKPERL), $(PERL) -cw $(p) ; )
	@-$(foreach p, $(CHECKPERL), $(PODCHECKER) $(p) ; )
	@-$(foreach p, $(CHECKPERL), $(PERLCRITIC) $(p) ; )
	@$(ECHO) 'noemail@example.com' | ./checker
	@$(MIMEDEFANG_PL) -f $(HBL_FILTER) -test
	@$(MIMEDEFANG_PL) -f $(HBL_FILTER) -validate

# Install Perl modules. Print instructions for manual setup.
install:
	@$(INSTALL) -CD -m=644 $(PERL_MODULES) $(PM_INSTALL_PATH)
	@$(ECHO) "!!! Manual steps are needed to install/setup MIMEDefang HASHBL milter !!!"
	@$(ECHO) "1. Copy/paste code from $(HBL_FILTER) into $(MIMEDEFANG_FILTER) file."
	@$(ECHO) "2. Read and follow README.md for additional MIMEDefang config/setup."
	@$(ECHO) "3. Restart mimedefang daemon/service to activate HASHBL milter."
